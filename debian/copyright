Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: pipenv
Source: https://github.com/pypa/pipenv
Files-Excluded: get-pipenv.py
                pipenv/patched/pip/_vendor/distlib/*.exe
Comment: These files were removed because
    get-pipenv.py: Unnecessary file that contains a copy of pip
    pipenv/patched/pip/_vendor/distlib/*.exe: Prebuilt windows binaries

Files: *
Copyright: 2017 Kenneth Reitz
License: MIT

Files: docs/_static/konami.js
Copyright: 2009 George Mandis
License: MIT

Files: pipenv/patched/pip/*
Copyright: 2012 Simon Sapin, 2008-2020 Andrey Petrov
License: MIT

Files: pipenv/patched/pip/_vendor/distro/*
Copyright: 2015-2017 Nir Cohen
License: Apache-2.0

Files: pipenv/patched/pip/_vendor/packaging/*
Copyright: 2014-2019 Donald Stufft
License: BSD-2-Clause or Apache-2.0

Files: pipenv/patched/pip/_vendor/colorama/*
Copyright: 2010-2013 Jonathan Hartley
License: BSD-3-Clause

Files: pipenv/patched/pip/_vendor/webencodings/*
Copyright: 2012 Simon Sapin
License: BSD-3-Clause

Files: pipenv/patched/pip/_vendor/distlib/*
Copyright: 2012-2021 The Python Software Foundation
License: Python

Files: pipenv/patched/pip/_vendor/distlib/index.py
       pipenv/patched/pip/_vendor/distlib/resources.py
       pipenv/patched/pip/_vendor/distlib/locators.py
       pipenv/patched/pip/_vendor/distlib/wheel.py
       pipenv/patched/pip/_vendor/distlib/scripts.py
       pipenv/patched/pip/_vendor/distlib/__init__.py
       pipenv/patched/pip/_vendor/distlib/compat.py
       pipenv/patched/pip/_vendor/distlib/markers.py
Copyright: 2012-2020 Vinay Sajip
License: Python

Files: pipenv/patched/pip/_vendor/pygments/*
Copyright: 2006-2022 Pygments team
License: BSD-2-Clause

Files: pipenv/patched/pip/_vendor/requests/*
Copyright: 2012-2017 Kenneth Reitz
License: Apache-2.0

Files: pipenv/patched/pip/_vendor/chardet/*
Copyright: 1998-2001 Netscape Communications Corporation
License: LGPL-2.1+

Files: pipenv/patched/pip/_vendor/chardet/hebrewprober.py
Copyright: 2005 Shy Shalom
License: LGPL-2.1+

Files: pipenv/patched/pip/_vendor/resolvelib/*
Copyright: 2018 Tzu-ping Chung <uranusjr@gmail.com>
License: ISC

Files: pipenv/patched/pip/_vendor/cachecontrol/*
Copyright: 2012-2021 Eric Larson
License: Apache-2.0

Files: pipenv/patched/pip/_vendor/tenacity/*
Copyright: 2016-2021 Julien Danjou
           2016 Joshua Harlow
           2013-2014 Ray Holder
           2016 Étienne Bersac
           2017 Elisey Zanko
License: Apache-2.0

Files: pipenv/patched/pip/_vendor/msgpack/*
Copyright: 2008-2011 INADA Naoki <songofacandy@gmail.com>
License: Apache-2.0

Files: pipenv/patched/pip/_vendor/pyparsing/*
Copyright: 2003-2022 Paul T. McGuire
License: MIT

Files: pipenv/patched/pip/_vendor/six*
Copyright: 2010-2020 Benjamin Peterson
License: MIT

Files: pipenv/patched/pip/_vendor/tomli/*
Copyright: 2021 Taneli Hukkinen
License: MIT

Files: pipenv/patched/pip/_vendor/truststore/*
Copyright: 2022 Seth Michael Larson
License: MIT

Files: pipenv/patched/pip/_vendor/platformdirs/*
Copyright: 2010 ActiveState Software Inc.
License: MIT

Files: pipenv/patched/pip/_vendor/urllib3/*
Copyright: 2008-2020 Andrey Petrov and contributors
License: MIT

Files: pipenv/patched/pip/_vendor/urllib3/packages/six.py
Copyright: 2010-2020 Benjamin Peterson
License: MIT

Files: pipenv/patched/pip/_vendor/urllib3/contrib/_securetransport/bindings.py
       pipenv/patched/pip/_vendor/urllib3/contrib/securetransport.py
Copyright: 2015-2016 Will Bond <will@wbond.net>
License: MIT

Files: pipenv/patched/pip/_vendor/idna/*
Copyright: 2013-2021 Kim Davies
License: BSD-3-Clause

Files: pipenv/patched/safety/*
Copyright: 2016 pyup.io
License: MIT

Files: pipenv/utils/requirementslib.py
Copyright: 2013 Mahmoud Hashemi
License: MIT

Files: pipenv/vendor/click/*
Copyright: 2001-2006 Gregory P. Ward
           2002-2006 Python Software Foundation
           2014 Pallets
License: BSD-3-Clause

Files: pipenv/vendor/click_didyoumean/*
Copyright: 2016 Timo Furrer
License: BSD-3-Clause

Files: pipenv/vendor/colorama/*
Copyright: 2013 Jonathan Hartley
License: BSD-3-Clause

Files: pipenv/vendor/dotenv/*
Copyright: 2014 Saurabh Kumar
           2013 Ted Tieken
           2013 Jacob Kaplan-Moss
License: BSD-3-Clause

Files: pipenv/vendor/dparse/*
Copyright: 2017 Jannis Gebauer
License: MIT

Files: pipenv/vendor/dparse/parser.py
Copyright: 2016 Jason R Coombs <jaraco@jaraco.com>
License: MIT

Files: pipenv/vendor/pexpect/*
Copyright: 2012-2014 Noah Spurrier, Pexpect development team
License: ISC

Files: pipenv/vendor/pipdeptree/*
Copyright: 2015 Vineet Naik <naikvin@gmail.com>
License: MIT

Files: pipenv/vendor/plette/*
       pipenv/vendor/shellingham/*
Copyright: 2018 Tzu-ping Chung <uranusjr@gmail.com>
License: ISC

Files: pipenv/vendor/ptyprocess/*
Copyright: 2012-2014 Noah Spurrier, Pexpect development team
License: ISC

Files: pipenv/vendor/pythonfinder/*
Copyright: 2016 Steve Dower, 2018 Dan Ryan
License: MIT

Files: pipenv/vendor/tomli/*
Copyright: 2021 Taneli Hukkinen
License: MIT

Files: pipenv/vendor/tomlkit/*
Copyright: 2018 Sébastien Eustace
License: MIT

Files: tests/fixtures/fake-package/*
Copyright: 2019 Dan Ryan <dan@danryan.co>
License: ISC

Files: debian/*
Copyright: 2018 Bastian Venthur <venthur@debian.org>
           2022-2024 Ileana Dumitrescu <ileanadumitrescu95@gmail.com>
License: MIT

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

License: BSD-3-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. Neither the name of the University nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
 A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE HOLDERS OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: BSD-2-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
 https://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian systems, the complete text of the Apache version 2.0 license
 can be found in "/usr/share/common-licenses/Apache-2.0".

License: Python
 Licensed under the Python Software License;
 .
 https://opensource.org/licenses/Python-2.0

License: ISC
 Permission to use, copy, modify, and/or distribute this software for any
 purpose with or without fee is hereby granted, provided that the above
 copyright notice and this permission notice appear in all copies.
 .
 THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
 REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
 AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
 INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 PERFORMANCE OF THIS SOFTWARE.

License: LGPL-2.1+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 02110-1301  USA
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License version 2.1 can be found in "/usr/share/common-licenses/LGPL-2.1".
